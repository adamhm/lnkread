//	lnkread
//
//	This is a basic tool for reading the target, arguments, working directory and icon information from Microsoft .lnk
//	files, for use by my Wine wrapper scripts. There's a lot of room for improvement and expansion but for now at least
//	it seems to be adequate enough for its intended purpose.
//
//	From spec: https://msdn.microsoft.com/en-us/library/dd871305.aspx
//
//	Usage:
//
//	First argument = .lnk file to read
//	Second argument = output format
//
//	Output is in the order: target / name / relative path / workingdir / arguments / icon / iconindex
//	Default/0 = one field per line with description, e.g. "Target: C:\test.exe"
//	1 = one field per line, no description: "C:\test.exe"
//	2 = all fields on one line separated by |

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <wchar.h>

#define BUFSIZE 10000

struct {
	uint32_t HeaderSize;
	struct {
		uint32_t CLSID1;
		uint32_t CLSID2;
		uint32_t CLSID3;
		uint32_t CLSID4;
	} LinkCLSID;
	struct {
		bool HasLinkTargetIDList : 1;
		bool HasLinkInfo : 1;
		bool HasName : 1;
		bool HasRelativePath : 1;
		bool HasWorkingDir : 1;
		bool HasArguments : 1;
		bool HasIconLocation : 1;
		bool IsUnicode : 1;
		bool ForceNoLinkInfo : 1;
		bool HasExpString : 1;
		bool RunInSeparateProcess : 1;
		bool Unused1 : 1;
		bool HasDarwinID : 1;
		bool RunAsUser : 1;
		bool HasExpIcon : 1;
		bool NoPidlAlias : 1;
		bool Unused2 : 1;
		bool RunWithShimLayer : 1;
		bool ForceNoLinkTrack : 1;
		bool EnableTargetMetadata : 1;
		bool DisableLinkPathTracking : 1;
		bool DisableKnownFolderTracking : 1;
		bool DisableKnownFolderAlias : 1;
		bool AllowLinkToLink : 1;
		bool UnaliasOnSave : 1;
		bool PreferEnvironmentPath : 1;
		bool KeepLocalIDListForUNCTarget : 1;
		unsigned int : 5;
	} LinkFlags;
	struct {
		bool FILE_ATTRIBUTE_READONLY : 1;
		bool FILE_ATTRIBUTE_HIDDEN : 1;
		bool FILE_ATTRIBUTE_SYSTEM : 1;
		bool Reserved1 : 1;
		bool FILE_ATTRIBUTE_DIRECTORY : 1;
		bool FILE_ATTRIBUTE_ARCHIVE : 1;
		bool Reserved2 : 1;
		bool FILE_ATTRIBUTE_NORMAL : 1;
		bool FILE_ATTRIBUTE_TEMPORARY : 1;
		bool FILE_ATTRIBUTE_SPARSE_FILE : 1;
		bool FILE_ATTRIBUTE_REPARSE_POINT : 1;
		bool FILE_ATTRIBUTE_COMPRESSED : 1;
		bool FILE_ATTRIBUTE_OFFLINE : 1;
		bool FILE_ATTRIBUTE_NOT_CONTENT_INDEXED : 1;
		bool FILE_ATTRIBUTE_ENCRYPTED : 1;
		unsigned int : 17;
	} FileAttributes;
	struct {
		uint32_t dwLowDateTime;
		uint32_t dwHighDateTime;
	} CreationTime;
	struct {
		uint32_t dwLowDateTime;
		uint32_t dwHighDateTime;
	} AccessTime;
	struct {
		uint32_t dwLowDateTime;
		uint32_t dwHighDateTime;
	} WriteTime;
	uint32_t FileSize;
	int32_t IconIndex;
	uint32_t ShowCommand;
	struct {
		uint8_t LowByte;
		uint8_t HighByte;
	} HotKey;
	uint16_t Reserved1;
	uint32_t Reserved2;
	uint32_t Reserved3;
} ShellLinkHeader;


int main(int argc, char **argv)
{
	uint16_t IDListSize, VolumeIDSize;
	uint32_t LinkInfoPos;
	char outputform=0, LocalBasePath[BUFSIZE], CommonPathSuffix[BUFSIZE], NAME_STRING[BUFSIZE], RELATIVE_PATH[BUFSIZE], WORKING_DIR[BUFSIZE], COMMAND_LINE_ARGUMENTS[BUFSIZE], ICON_LOCATION[BUFSIZE];

	FILE *fp;

	if (argc < 2) {
		printf("Reads basic information from Microsoft .lnk files. Usage:\n\n	lnkread <file> <output format>\n\n");
		return 1;
	}

	if (argc >2)
		outputform=atoi(argv[2]);

	if ((fp = fopen(argv[1], "r+b")) == 0) {
		fprintf(stderr, "ERROR: could not open %s\n", argv[1]);
		return 1;
	}

	fread(&ShellLinkHeader, sizeof(ShellLinkHeader), 1, fp);

	if (ShellLinkHeader.HeaderSize != 0x4C
	|| ShellLinkHeader.LinkCLSID.CLSID1 != 0x21401
	|| ShellLinkHeader.LinkCLSID.CLSID2 != 0
	|| ShellLinkHeader.LinkCLSID.CLSID3 != 0xC0
	|| ShellLinkHeader.LinkCLSID.CLSID4 != 0x46000000) {
		fprintf(stderr, "File does not appear to be a Microsoft .lnk file\n");
		return 1;
	}

	if (ShellLinkHeader.LinkFlags.HasLinkTargetIDList) { 		// Ignore LinkTargetIDList
		fread(&IDListSize, sizeof(IDListSize), 1, fp);
		fseek(fp, IDListSize, SEEK_CUR);
	}

	if (ShellLinkHeader.LinkFlags.HasLinkInfo) {
		LinkInfoPos=ftell(fp);
		uint32_t LinkInfoSize, LinkInfoHeaderSize;
		struct {
			bool VolumeIDAndLocalBasePath : 1;
			bool CommonNetworkRelativeLinkAndPathSuffix : 1;
			unsigned int : 30;
		} LinkInfoFlags;
		uint32_t VolumeIDOffset, LocalBasePathOffset, CommonNetworkRelativeLinkOffset, CommonPathSuffixOffset;
		int32_t LocalBasePathOffsetUnicode, CommonPathSuffixOffsetUnicode;
		uint16_t CharCount;
		int16_t UnicodeChar;
		int16_t UnicodeBuffer[BUFSIZE];

		fread(&LinkInfoSize, sizeof(LinkInfoSize), 1, fp);
		fread(&LinkInfoHeaderSize, sizeof(LinkInfoHeaderSize), 1, fp);
		fread(&LinkInfoFlags, sizeof(LinkInfoFlags), 1, fp);
		fread(&VolumeIDOffset, sizeof(VolumeIDOffset), 1, fp);
		fread(&LocalBasePathOffset, sizeof(LocalBasePathOffset), 1, fp);
		fread(&CommonNetworkRelativeLinkOffset, sizeof(CommonNetworkRelativeLinkOffset), 1, fp);
		fread(&CommonPathSuffixOffset, sizeof(CommonPathSuffixOffset), 1, fp);
		if (LinkInfoHeaderSize >= 0x24) {
			fread(&LocalBasePathOffsetUnicode, sizeof(LocalBasePathOffsetUnicode), 1, fp);
			fread(&CommonPathSuffixOffsetUnicode, sizeof(CommonPathSuffixOffsetUnicode), 1, fp);
		}

		if (LinkInfoFlags.VolumeIDAndLocalBasePath) {
			fseek(fp, (LinkInfoPos+LocalBasePathOffset), SEEK_SET);
			fgets(LocalBasePath,(LinkInfoSize-LocalBasePathOffset),fp);
		}

		fseek(fp, (LinkInfoPos+CommonPathSuffixOffset), SEEK_SET);
		fgets(CommonPathSuffix,(LinkInfoSize-CommonPathSuffixOffset),fp);

		if (strlen(CommonPathSuffix) > 0)
			strcat(LocalBasePath,CommonPathSuffix);

		fseek(fp, (LinkInfoPos+LinkInfoSize), SEEK_SET);
		if (ShellLinkHeader.LinkFlags.HasName) {
			fread(&CharCount, sizeof(CharCount), 1, fp);
			if (CharCount > BUFSIZE || fread(&UnicodeBuffer, sizeof(*UnicodeBuffer), CharCount, fp) != CharCount) {
				fprintf(stderr, "An error occurred reading NAME_STRING from file; aborting\n");
				return 1;
			}
			for (int X=0 ; X < CharCount; X++)
				NAME_STRING[X]=wctob(UnicodeBuffer[X]);
			NAME_STRING[CharCount]=0;
		}
		if (ShellLinkHeader.LinkFlags.HasRelativePath) {
			fread(&CharCount, sizeof(CharCount), 1, fp);
			if (CharCount > BUFSIZE || fread(&UnicodeBuffer, sizeof(*UnicodeBuffer), CharCount, fp) != CharCount) {
				fprintf(stderr, "An error occurred reading RELATIVE_PATH from file; aborting\n");
				return 1;
			}
			for (int X=0 ; X < CharCount; X++)
				RELATIVE_PATH[X]=wctob(UnicodeBuffer[X]);
			RELATIVE_PATH[CharCount]=0;
		}
		if (ShellLinkHeader.LinkFlags.HasWorkingDir) {
			fread(&CharCount, sizeof(CharCount), 1, fp);
			if (CharCount > BUFSIZE || fread(&UnicodeBuffer, sizeof(*UnicodeBuffer), CharCount, fp) != CharCount) {
				fprintf(stderr, "An error occurred reading WORKING_DIR from file; aborting\n");
				return 1;
			}
			for (int X=0 ; X < CharCount; X++)
				WORKING_DIR[X]=wctob(UnicodeBuffer[X]);
			WORKING_DIR[CharCount]=0;
		}
		if (ShellLinkHeader.LinkFlags.HasArguments) {
			fread(&CharCount, sizeof(CharCount), 1, fp);
			if (CharCount > BUFSIZE || fread(&UnicodeBuffer, sizeof(*UnicodeBuffer), CharCount, fp) != CharCount) {
				fprintf(stderr, "An error occurred reading COMMAND_LINE_ARGUMENTS from file; aborting\n");
				return 1;
			}
			for (int X=0 ; X < CharCount; X++)
				COMMAND_LINE_ARGUMENTS[X]=wctob(UnicodeBuffer[X]);
			COMMAND_LINE_ARGUMENTS[CharCount]=0;
		}
		if (ShellLinkHeader.LinkFlags.HasIconLocation) {
			fread(&CharCount, sizeof(CharCount), 1, fp);
			if (CharCount > BUFSIZE || fread(&UnicodeBuffer, sizeof(*UnicodeBuffer), CharCount, fp) != CharCount) {
				fprintf(stderr, "An error occurred reading ICON_LOCATION from file; aborting\n");
				return 1;
			}
			for (int X=0 ; X < CharCount; X++)
				ICON_LOCATION[X]=wctob(UnicodeBuffer[X]);
			ICON_LOCATION[CharCount]=0;
		}
		switch (outputform) {
			case 1:
				printf("%s\n%s\n%s\n%s\n%s\n%s\n%d\n", LocalBasePath, NAME_STRING, RELATIVE_PATH, WORKING_DIR, COMMAND_LINE_ARGUMENTS, ICON_LOCATION, ShellLinkHeader.IconIndex);
				break;
			case 2:
				printf("%s|%s|%s|%s|%s|%s|%d\n", LocalBasePath, NAME_STRING, RELATIVE_PATH, WORKING_DIR, COMMAND_LINE_ARGUMENTS, ICON_LOCATION, ShellLinkHeader.IconIndex);
				break;
			default:
				printf("Target: %s\nName: %s\nRelative path: %s\nWorking Directory: %s\nArguments: %s\nIcon: %s\nIconIndex: %d\n", LocalBasePath, NAME_STRING, RELATIVE_PATH, WORKING_DIR, COMMAND_LINE_ARGUMENTS, ICON_LOCATION, ShellLinkHeader.IconIndex);
				break;
		}
		return 0;
	}
	fprintf(stderr, "ERROR: Cannot read link (no LinkInfo entry).\n");
	return 1;
}
